import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:study_app/configs/themes/app_colors.dart';
import 'package:study_app/utils/dimensions.dart';
import 'package:study_app/widgets/app_circle_button.dart';

class AppIntroductionScreen extends StatelessWidget {
  const AppIntroductionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        decoration: BoxDecoration(
          gradient: mainGradient(),
        ),
        padding: EdgeInsets.only(
          left: Get.width * 0.2,
          right: Get.width * 0.2,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.star,
              size: 65,
            ),
            SizedBox(
              height: 40,
            ),
            Text(
              'This is a study app you can use it as you want. If you understand how this works, you would be able to scale it. With this you will master firebase backend and flutter front end.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                color: onSurfaceTextColor,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            AppCircleButton(
              onTap: () => Get.offAndToNamed('/home'),
              child: Icon(
                Icons.arrow_forward,
                size: 35,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
