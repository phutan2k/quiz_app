import 'package:flutter/material.dart';
import 'package:get/get.dart';

const double _mobileScreenPadding = 25.0;
const double _cardBorderRadius = 10.0;

double get mobileScreenPadding => _mobileScreenPadding;

double get cardBorderRadius => _cardBorderRadius;

class UIParameters {
  static BorderRadius get cardBorderRadius =>
      BorderRadius.circular(_cardBorderRadius);

  static EdgeInsets get mobileScreenPadding =>
      const EdgeInsets.all(_mobileScreenPadding);

  /**
   * TODO: Theme.of(context).brightness
   * là một phương thức được sử dụng để truy cập đến độ sáng
   * của chủ đề (theme) hiện tại trong widget tree
   */
  static bool isDarkMode() {
    return Get.isDarkMode ? true : false;
  }
}
