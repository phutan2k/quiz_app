import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:study_app/firebase_ref/references.dart';
import 'package:study_app/models/question_paper_model.dart';
import 'package:study_app/screens/home/home_screen.dart';
import 'package:study_app/screens/login/login_screen.dart';
import 'package:study_app/widgets/dialogs/dialogue_widget.dart';

class AuthController extends GetxController {
  @override
  void onReady() {
    initAuth();
    super.onReady();
  }

  late FirebaseAuth _auth;

  final _user = Rxn<User>();
  late Stream<User?> _authStateChange;

  var batch = fireStore.batch();

  void initAuth() async {
    await Future.delayed(Duration(seconds: 2));
    /**
     * TODO: Firebase Authen
     */
    _auth = FirebaseAuth.instance;
    _authStateChange = _auth.authStateChanges();
    /**
     * TODO: Các sự kiện được kích hoạt khi xảy ra các trường hợp sau:
     * _ Ngay sau khi listener (người dùng) đã được đăng ký.
     * _ Khi người dùng đăng nhập
     * _ Khi người dùng hiện tại đã đăng xuất.
     */
    _authStateChange.listen((User? user) {
      _user.value = user;
    });
    navigateToIntroduction();
  }

  signInWithGoogle() async {
    // send credentials to firebase
    final GoogleSignIn _googleSignIn = GoogleSignIn();
    try {
      GoogleSignInAccount? account = await _googleSignIn.signIn();

      if (account != null) {
        final _authAccount = await account.authentication;

        final _credential = GoogleAuthProvider.credential(
          idToken: _authAccount.idToken,
          accessToken: _authAccount.accessToken,
        );

        await _auth.signInWithCredential(_credential);
        await saveUser(account);

        print('Account: ${account.email}');
        navigateToHomePage();
      }
    } on Exception catch (error) {
      // AppLogger.e(error);
      print('The error is: $error');
    }
  }

  User? getUser() {
    _user.value = _auth.currentUser;
    return _user.value;
  }

  saveUser(GoogleSignInAccount account) {
    batch.set(userRF.doc(account.email), {
      'email': account.email,
      'name': account.displayName,
      'profilepic': account.photoUrl,
    });
  }

  Future<void> signOut() async {
    print('Sign out');
    try {
      await _auth.signOut();
      navigateToHomePage();
    } on FirebaseAuthException catch (e) {
      print('Error logout: $e');
    }
  }

  void navigateToIntroduction() {
    Get.offAllNamed('/introduction');
  }

  navigateToHomePage() {
    Get.offAllNamed(HomeScreen.routeName);
  }

  void showLoginAlertDialogue() {
    Get.dialog(
      Dialogs.questionStartDialogue(onTap: () {
        Get.back();
        navigateToLoginPage();
      }),
      // Don't allow users to dismiss Dialog
      // by clicking on the background
      barrierDismissible: false,
    );
  }

  void navigateToLoginPage() {
    Get.toNamed(LoginScreen.routeName);
  }

  bool isLoggedIn() {
    return _auth.currentUser != null;
  }
}
