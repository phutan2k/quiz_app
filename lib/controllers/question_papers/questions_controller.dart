import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:study_app/controllers/auth_controller.dart';
import 'package:study_app/controllers/question_papers/question_paper_controller.dart';
import 'package:study_app/controllers/question_papers/questions_controller_extension.dart';
import 'package:study_app/firebase_ref/loading_status.dart';
import 'package:study_app/models/question_paper_model.dart';
import 'package:study_app/screens/home/home_screen.dart';
import 'package:study_app/screens/question/result_screen.dart';

import '../../firebase_ref/references.dart';

class QuestionController extends GetxController {
  final loadingStatus = LoadingStatus.loading.obs;
  late QuestionPaperModel questionPaperModel;
  final allQuestions = <Questions>[];
  final questionIndex = 0.obs;

  bool get isFirstQuestion => questionIndex.value > 0;

  bool get isLastQuestion => questionIndex.value >= allQuestions.length - 1;

  Rxn<Questions> currentQuestion = Rxn<Questions>();

  // Timer
  Timer? _timer;
  int remainSeconds = 1;
  final time = '00.00'.obs;

  @override
  void onReady() {
    /**
     * TODO: take data from QuestionPaperController by arguments
     */
    final _questionPaper = Get.arguments as QuestionPaperModel;
    print('...Ready...');

    // TODO: Load data from firebase
    loadData(_questionPaper);
    super.onReady();
  }

  void loadData(QuestionPaperModel questionPaper) async {
    questionPaperModel = questionPaper;
    loadingStatus.value = LoadingStatus.loading;
    try {
      /**
       * QuerySnapshot: Đại diện cho một tập hợp các tài liệu được
       * trả về từ một truy vấn Firestore. Mỗi QuerySnapshot
       * chứa một danh sách các QueryDocumentSnapshot
       * TODO: .get() sẽ trả về sub-document của collection
       */
      final QuerySnapshot<Map<String, dynamic>> questionQuery =
          await questionPaperRF
              .doc(questionPaper.id)
              .collection('questions')
              .get();
      /**
       * questionQuery.docs sẽ trả về một danh sách các đối tượng QueryDocumentSnapshot
       * QueryDocumentSnapshot, đại diện cho từng tài liệu trong kết quả truy vấn.
       */
      final questions = questionQuery.docs
          .map((snapshot) => Questions.fromSnapshot(snapshot))
          .toList();

      questionPaper.questions = questions;

      /**
       * TODO: Tại sao xét vòng for cho sub-document này?
       * Vì document đầu tiền được truy cập bởi id của model
       * _ sub-document: 5 document => duyệt từng document
       * trong list sub-document
       *
       * TODO: .get() để lấy sub-document của collection answers
       */
      for (Questions _question in questionPaper.questions!) {
        final QuerySnapshot<Map<String, dynamic>> answersQuery =
            await questionPaperRF
                .doc(questionPaper.id)
                .collection('questions')
                .doc(_question.id)
                .collection('answers')
                .get();
        final answers = answersQuery.docs
            .map((answer) => Answers.fromSnapshot(answer))
            .toList();
        _question.answers = answers;
      }
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
    }
    /**
     * TODO: assignAll() - Replaces all existing items of this list with items
     */
    if (questionPaper.questions != null &&
        questionPaper.questions!.isNotEmpty) {
      allQuestions.assignAll(questionPaper.questions!);
      currentQuestion.value = questionPaper.questions![0];

      // TODO: Timer
      _startTimer(questionPaper.timeSeconds);
      print('...startTimer...');

      if (kDebugMode) {
        print(questionPaper.questions![0].question);
      }
      // update state
      loadingStatus.value = LoadingStatus.completed;
    } else {
      loadingStatus.value = LoadingStatus.error;
    }
  }

  void selectedAnswer(String? answer) {
    currentQuestion.value!.selectedAnswer = answer;
    /**
     * TODO: ['answers_list'] chỉ là cách GetX xác định rằng observable
     * TODO: được gọi là "answer_list" cần được cập nhật
     */
    update(['answers_list', 'answer_review_list']);
  }

  String get completedTest {
    // TODO: Trả về số lượng câu trả lời đúng trong list
    final answered = allQuestions
        .where((element) => element.selectedAnswer != null)
        .toList()
        .length;
    return '$answered out of ${allQuestions.length} answered';
  }

  /**
   * Phương thức được gọi khi click vào các câu trả lời
   * ở màn test_overview_screen
   */
  void jumpToQuestion(int index, {bool isGoBack = true}) {
    questionIndex.value = index;
    currentQuestion.value = allQuestions[index];
    if (isGoBack) {
      Get.back();
    }
  }

  void nextQuestion() {
    if (questionIndex.value >= allQuestions.length - 1) return;
    questionIndex.value++;
    currentQuestion.value = allQuestions[questionIndex.value];
  }

  void prevQuestion() {
    if (questionIndex.value <= 0) return;
    questionIndex.value--;
    currentQuestion.value = allQuestions[questionIndex.value];
  }

  _startTimer(int seconds) {
    const duration = Duration(seconds: 1);
    remainSeconds = seconds;
    /**
     * Timer.periodic() là một phương thức được sử dụng để tạo ra một
     * Timer có thể lặp lại theo khoảng thời gian nhất định.
     * Khi bạn sử dụng Timer.periodic(), bạn có thể chỉ định một hàm
     * hoặc một hàm callback sẽ được gọi sau mỗi khoảng thời gian đã cho.
     *
     * _ duration: Đối tượng Duration xác định khoảng thời gian
     * giữa các lần gọi hàm callback.
     * _ callback: Một hàm hoặc hàm callback sẽ được gọi sau mỗi
     * khoảng thời gian đã cho.
     */
    _timer = Timer.periodic(duration, (Timer timer) {
      if (remainSeconds == 0) {
        timer.cancel();
      } else {
        int minutes = remainSeconds ~/ 60;
        int seconds = remainSeconds % 60;
        time.value = minutes.toString().padLeft(2, '0') +
            ':' +
            seconds.toString().padLeft(2, '0');
        remainSeconds--;
      }
    });
  }

  void complete() {
    _timer!.cancel();
    Get.offAndToNamed(ResultScreen.routeName);
  }

  void navigateToHome() {
    _timer!.cancel();
    Get.offNamedUntil(HomeScreen.routeName, (route) => false);
  }

  void tryAgain() {
    Get.find<QuestionPaperController>()
        .navigateToQuestion(paper: questionPaperModel, tryAgain: true);
  }
}
