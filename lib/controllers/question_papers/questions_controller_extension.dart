import 'package:firebase_auth/firebase_auth.dart';
import 'package:study_app/controllers/question_papers/questions_controller.dart';
import 'package:get/get.dart';
import 'package:study_app/models/question_paper_model.dart';
import '../../firebase_ref/references.dart';
import '../auth_controller.dart';

/**
 * Vì QuestionsControllerExtension là extension của QuestionController
 * nên instance của QuestionController cũng trỏ được đến
 * QuestionsControllerExtension
 */
extension QuestionsControllerExtension on QuestionController {
  int get correctQuestionCount => allQuestions
      .where((element) => element.selectedAnswer == element.correctAnswer)
      .toList()
      .length;

  String get correctAnsweredQuestions {
    return '$correctQuestionCount out of ${allQuestions.length} are correct';
  }

  String get points {
    var points = (correctQuestionCount / allQuestions.length) *
        100 *
        (questionPaperModel.timeSeconds - remainSeconds) /
        questionPaperModel.timeSeconds *
        100;

    return points.toStringAsFixed(2);
  }

  Future<void> saveTestResults() async {
    var batch = fireStore.batch();
    User? _user = Get.find<AuthController>().getUser();

    if (_user == null) return;
    batch.set(
      userRF
          .doc(_user.email)
          .collection('my_recent_tests')
          .doc(questionPaperModel.id),
      {
        'points': points,
        'correct_answer': '$correctQuestionCount/${allQuestions.length}',
        'question_id': questionPaperModel.id,
        'time': questionPaperModel.timeSeconds - remainSeconds,
      },
    );
    batch.commit();
    navigateToHome();
  }
}
