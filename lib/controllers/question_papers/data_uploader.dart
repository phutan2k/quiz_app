import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:study_app/firebase_ref/loading_status.dart';
import 'package:study_app/firebase_ref/references.dart';
import 'package:study_app/models/question_paper_model.dart';

class DataUploader extends GetxController {
  @override
  void onReady() {
    uploadData();
    super.onReady();
  }

  /**
   * obs - là 1 đối tượng thuộc loại Observable
   * Observable cho phép theo dõi và cập nhật trạng thái của một biến
   */
  final loadingStatus = LoadingStatus.loading.obs; // loadingStatus is obs

  Future<void> uploadData() async {
    loadingStatus.value = LoadingStatus.loading; // 0

    // Initialize an instance of Cloud Firestore
    final fireStore = FirebaseFirestore.instance;
    /**
     * TODO: AssetManifest.json - khi build project, Flutter sẽ
     * tự động tạo ra tệp AssetManifest.json
     * TODO: AssetManifest.json gồm tất cả các file được add vào file assets
     */
    final manifestContent = await DefaultAssetBundle.of(Get.context!)
        .loadString('AssetManifest.json');
    final Map<String, dynamic> manifestMap = json.decode(manifestContent);
    print('All files of assets: ${manifestMap.keys}');

    // load json file and print path
    final papersInAssets = manifestMap.keys
        .where((path) =>
            path.startsWith('assets/DB/papers') && path.contains('.json'))
        .toList();
    print('Papers in Assets: $papersInAssets');

    List<QuestionPaperModel> questionPapers = [];

    print('');
    print('Content file paper in assets: ');
    for (var paper in papersInAssets) {
      String stringPaperContent = await rootBundle.loadString(paper);
      print(stringPaperContent);

      questionPapers
          .add(QuestionPaperModel.fromJson(json.decode(stringPaperContent)));
    }

    var batch = fireStore.batch();

    /**
     * TODO: Create data in Cloud Firestore
     */
    for (var paper in questionPapers) {
      /**
       * questionPaperRF.doc(paper.id): tham chiếu đến
       * document (sub-collection) có id là paper.id
       *
       * collection: _ document (sub-collection)
       *             _ field
       */
      batch.set(questionPaperRF.doc(paper.id), {
        'title': paper.title,
        'image_url': paper.imageUrl,
        'description': paper.description,
        'time_seconds': paper.timeSeconds,
        'questions_count': paper.questions == null ? 0 : paper.questions!.length
      });

      for (var questions in paper.questions!) {
        final questionPath =
            questionRF(paperId: paper.id, questionId: questions.id);

        batch.set(questionPath, {
          'question': questions.question,
          'correct_answer': questions.correctAnswer,
        });

        for (var answer in questions.answers) {
          batch.set(questionPath.collection('answers').doc(answer.identifier), {
            'identifier': answer.identifier,
            'answer': answer.answer,
          });
        }
      }
    }
    await batch.commit();
    loadingStatus.value = LoadingStatus.completed;
  }
}
