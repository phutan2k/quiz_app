import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:study_app/controllers/auth_controller.dart';
import 'package:study_app/models/question_paper_model.dart';
import 'package:study_app/screens/question/questions_screen.dart';

import '../../firebase_ref/references.dart';
import '../../services/firebase_storage_service.dart';

class QuestionPaperController extends GetxController {
  final allPaperImages = <String>[].obs;
  final allPapers = <QuestionPaperModel>[].obs;

  @override
  void onReady() {
    getAllPapers();
    super.onReady();
  }

  Future<void> getAllPapers() async {
    // List<String> imgName = ['biology', 'chemistry', 'maths', 'physics'];
    try {
      /**
       * TODO: get data from firebase
       */
      QuerySnapshot<Map<String, dynamic>> data = await questionPaperRF.get();
      // Gets a list of all the documents in collection: 4 documents
      final paperList = data.docs
          .map((paper) => QuestionPaperModel.fromSnapshot(paper))
          .toList();
      allPapers.assignAll(paperList);

      for (var paper in paperList) {
        final imgUrl =
            await Get.find<FirebaseStorageService>().getImage(paper.title);
        paper.imageUrl = imgUrl;
      }
      allPapers.assignAll(paperList);
    } catch (e) {
      print(e);
    }
  }

  void navigateToQuestion({
    required QuestionPaperModel paper,
    bool tryAgain = false,
  }) {
    AuthController _authController = Get.find();
    if (_authController.isLoggedIn()) {
      if (tryAgain) {
        Get.back();
        Get.toNamed(QuestionScreen.routeName,
            arguments: paper, preventDuplicates: false);
      } else {
        // TODO: pass data by arguments in GetX
        Get.toNamed(QuestionScreen.routeName, arguments: paper);
      }
    } else {
      print('The title is: ${paper.title}');
      _authController.showLoginAlertDialogue();
    }
  }
}
