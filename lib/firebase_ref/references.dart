import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

final fireStore = FirebaseFirestore.instance;

/**
 * fireStore.collection() : là một phương thức của Firestore API được
 * sử dụng để tham chiếu đến một bộ sưu tập (collection)
 * trong cơ sở dữ liệu Firestore
 */
final questionPaperRF = fireStore.collection('questionPapers');
final userRF = fireStore.collection('users');

/**
 * Function - Là một khối mã độc lập, có thể được gọi và sử dụng
 * bất cứ đâu trong chương trình.
 */
DocumentReference questionRF({
  required String paperId,
  required String questionId,
}) =>
    questionPaperRF.doc(paperId).collection('questions').doc(questionId);

// Function
Reference get firebaseStorage => FirebaseStorage.instance.ref();



